#pragma once
#include "pc.h"

#if !defined(CELL_CHR)
#define CELL_CHR ('o')
#endif
#if !defined(NO_CELL_CHR)
#define NO_CELL_CHR ('-')
#endif

/*
Initialize an empty (no cells) board game with the specified dimensions.
*/
int initGame(bool*** gamePtr, const size_t rows, const size_t cols);

/*
This function receives a game board matrix and its dimensions; it then plays a turn in the Game of Life.
*/
int advanceGame(bool** game, const size_t rows, const size_t cols);

/*
Print the game board.
*/
void printGame(const bool** game, const size_t rows, const size_t cols);

/*
This function receives a pointer to a board game matrix and its row count; it then frees its memory and sets the game board to NULL.
*/
void deleteGame(bool*** gamePtr, const size_t rows);
