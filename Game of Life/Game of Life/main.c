#include "game_of_life.h"
#if !defined(SLEEP_INTERVAL)
#define SLEEP_INTERVAL (300)
#endif

void cleanup();

#define INPUT(prompt, numberOfInputs, format, ...) \
{ \
	printf(prompt); \
	while (numberOfInputs != scanf_s(format, __VA_ARGS__)) \
	{ \
		cleanStdin(); \
		printf(prompt); \
	} \
}

bool** cellMatrix = NULL;
size_t rows = 0, cols = 0;
char* buffer = NULL;

int main(int argc, char** argv)
{
	int retVal = EXIT_SUCCESS;
	float sleepInterval = SLEEP_INTERVAL;
	bool* row = NULL;
	size_t i = 0, j = 0;
	size_t bufferSize = 0;
	
	if (2 <= argc && 1 != sscanf_s(argv[1], "%d", &sleepInterval))
	{
		fprintf(stderr, "Usage: %s [sleep-interval-between-rounds-in-ms]\n", *argv);
		exit(EXIT_FAILURE);
	}

#if defined(__linux__)
	/*Convert from milliseconds to seconds.*/
	sleepInterval /= 1000;
#endif

	atexit(cleanup);
	signal(SIGINT, exit);

	INPUT("Enter number of rows: ", 1, "%llu", &rows);
	INPUT("Enter number of columns: ", 1, "%llu", &cols);

	if (NULL == (buffer = malloc(bufferSize = (cols + 1) * sizeof(char))))
	{
		retVal = errno;
		fprintf(stderr, "Allocation of buffer failed!\n");
		exit(retVal);
	}

	if (EXIT_SUCCESS != initGame(&cellMatrix, rows, cols))
	{
		retVal = errno;
		fprintf(stderr, "Initialization of game board failed!\n");
		exit(retVal);
	}

	printf("Enter the starting board %llux%llu:\n", rows, cols);
	cleanStdin();
	for (i = 0; i < rows; ++i)
	{
		row = cellMatrix[i];
		if (NULL == fgets(buffer, bufferSize, stdin))
		{
			retVal = errno;
			fprintf(stderr, "Row #%llu input failed!\n", i + 1);
			exit(retVal);
		}
		cleanStdin();
		for (j = 0; j < cols; ++j)
			if (CELL_CHR == buffer[j])
				row[j] = true;
	}

	while (true)
	{
		printf("\n\n");
		printGame(cellMatrix, rows, cols);
		if (EXIT_SUCCESS != (retVal = advanceGame(cellMatrix, rows, cols)))
			exit(retVal);
#if defined(_WIN32) || defined(_WIN64)
		Sleep(sleepInterval);
#endif
#if defined(__linux__)
		sleep(sleepInterval);
#endif
	}
}

void cleanup()
{
	if (NULL != cellMatrix)
		deleteGame(&cellMatrix, rows);
	if (NULL != buffer)
	{
		free(buffer);
		buffer = NULL;
	}
#if defined(_DEBUG)
	fprintf(stderr, "\n\nLeaks: %d\n", _CrtDumpMemoryLeaks());
#endif
}
