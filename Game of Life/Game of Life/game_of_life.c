#include "game_of_life.h"

/*
Count how many live cells there are around a cell.
*/
inline size_t countLiveCellsAround(const bool** game, const size_t rows, const size_t cols, const size_t row, const size_t col);

/*
Try create a copy of the board. Returns NULL if the copy failed.
*/
bool** copyBoard(const bool** game, const size_t rows, const size_t cols);

int initGame(bool*** gamePtr, const size_t rows, const size_t cols)
{
	size_t i = 0, j = 0;
	const size_t rowSize = cols * sizeof(bool);
	bool** game = NULL, * row = NULL;

	if (NULL == gamePtr)
		return EXIT_FAILURE;
	if (NULL == (*gamePtr = game = malloc(rows * sizeof(bool*))))
	{
		deleteGame(gamePtr, rows);
		return EXIT_FAILURE;
	}

	for (i = 0; i < rows; ++i)
	{
		if (NULL == (game[i] = row = malloc(rowSize)))
		{
			deleteGame(gamePtr, rows);
			return EXIT_FAILURE;
		}
		for (j = 0; j < cols; ++j)
			row[j] = false;
	}
	return EXIT_SUCCESS;
}

int advanceGame(bool** game, const size_t rows, const size_t cols)
{
	size_t row = 0, col = 0, i = 0, j = 0, liveCellCount = 0;
	bool** copy = copyBoard(game, rows, cols), * gameRow = NULL,
		* copyRow = NULL, live = false;

	if (NULL == copy)
	{
		fprintf(stderr, "Failed to copy board!\n");
		return EXIT_FAILURE;
	}

	for (; row < cols; ++row)
	{
		gameRow = game[row];
		copyRow = copy[row];

		for (col = 0; col < cols; ++col)
		{
			live = copyRow[j];
			liveCellCount = countLiveCellsAround(game, rows, cols, row, col);
			gameRow[j] = (!live && 3 == liveCellCount)
				|| (live && 2 <= liveCellCount && liveCellCount <= 3);
		}
	}

	deleteGame(&copy, rows);
	return EXIT_SUCCESS;
}

void printGame(const bool** game, const size_t rows, const size_t cols)
{
	size_t i = 0, j = 0;
	bool* gameRow = NULL;

	if (NULL == game)
		return;

	for (; i < rows; ++i)
	{
		gameRow = game[i];
		for (j = 0; j < cols; ++j)
			putchar(gameRow[j] ? CELL_CHR : NO_CELL_CHR);
		putchar('\n');
	}
}

void deleteGame(bool*** gamePtr, const size_t rows)
{
	size_t i = 0;
	bool** game = NULL;

	if (NULL == gamePtr)
		return;
	if (NULL == (game = *gamePtr))
	{
		*gamePtr = NULL;
		return;
	}
	for (i = 0; i < rows; ++i)
	{
		if (NULL == game[i])
			continue;
		free(game[i]);
		game[i] = NULL;
	}
	free(game);
	*gamePtr = NULL;
}

inline size_t countLiveCellsAround(const bool** game, const size_t rows, const size_t cols, const size_t row, const size_t col)
{
	size_t liveCellCount = 0;
	size_t i = 0, j = 0;
	bool* gameRow = NULL;
	const size_t firstRow = row > 0 ? row - 1 : row,
		lastRow = row < rows - 1 ? row + 1 : row,
		firstCol = col > 0 ? col - 1 : col,
		lastCol = col < cols - 1 ? col + 1 : col;

	for (i = firstRow; i <= lastRow; ++i)
	{
		gameRow = game[i];
		for (j = firstCol; j <= lastCol; ++j)
		{
			/* Don't count cell itself */
			if (row == i && col == j)
				continue;

			if (gameRow[j])
				++liveCellCount;
		}
	}
	return liveCellCount;
}

bool** copyBoard(const bool** game, const size_t rows, const size_t cols)
{
	size_t i = 0, j = 0;
	bool** copy = NULL, * gameRow = NULL, * copyRow = NULL;

	initGame(&copy, rows, cols);

	if (NULL == copy)
		return NULL;

	for (; i < rows; ++i)
	{
		gameRow = game[i];
		copyRow = copy[i];

		for (j = 0; j < cols; ++j)
			copyRow[j] = gameRow[j];
	}

	return copy;
}
