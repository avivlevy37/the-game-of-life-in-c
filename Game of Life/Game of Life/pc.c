#include "pc.h"

void cleanStdin()
{
	char chr;
	do
	{
		chr = getchar();
		if (EOF == chr)
			exit(errno);
	} while ('\n' != chr);
}