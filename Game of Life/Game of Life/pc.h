#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <signal.h>

#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif
#if defined(__linux__)
#include <unistd.h>
#endif

void cleanStdin();
